export const JWT_SECRET_KEY: string;
export const GOOGLE_CLIENT_ID: string;
export const MONGO_URI: string;
export const NODEMAILER_GOOGLE_CLIENT_ID: string;
export const NODEMAILER_GOOGLE_CLIENT_SECRET: string;
export const NODEMAILER_USERNAME: string;
export const NODEMAILER_PASSWORD: string;
