# PracowniaDyplomowaSerwer

[Pracownia dyplomowa, dr Drozda 2019](https://gitlab.com/Mateusz_Medrek/pracowniadyplomowa)

[Pracownia dyplomowa - serwer, dr Drozda 2019](https://gitlab.com/Mateusz_Medrek/pracowniadyplomowaserwer)

### Jaka aplikacja

Aplikacja mobilna do zarządzania listą zadań

### Opis

Aplikacja umożliwia zapisywanie zadań do wykonania na dany dzień, jeśli w danym daniu są jakieś zadania do wykonania, aplikacja wysyła powiadomienie na telefon użytkownika, użytkownik po wejściu w powiadomienie ma pokazane zadania, może je oznaczyć jako wykonane gdy je wykona. Aplikacja pozwala na edycję treści oraz daty wykonania danego zadania
		
### Technologie

**React Native**, **Node.js**, **MongoDB**