import { OAuth2Client } from 'google-auth-library';
import nodemailer from 'nodemailer';

import {
  NODEMAILER_USERNAME,
  NODEMAILER_GOOGLE_CLIENT_ID,
  NODEMAILER_GOOGLE_CLIENT_SECRET,
} from '../../config/keys';

const oauth2Client = new OAuth2Client({
  clientId: NODEMAILER_GOOGLE_CLIENT_ID,
  clientSecret: NODEMAILER_GOOGLE_CLIENT_SECRET,
});
const transporter = nodemailer.createTransport({
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    type: 'OAuth2',
    user: NODEMAILER_USERNAME,
    clientId: NODEMAILER_GOOGLE_CLIENT_ID,
    clientSecret: NODEMAILER_GOOGLE_CLIENT_SECRET,
    accessToken: oauth2Client.credentials.access_token,
    refreshToken: oauth2Client.credentials.refresh_token,
    expires: oauth2Client.credentials.expiry_date,
  },
});

export default transporter;
