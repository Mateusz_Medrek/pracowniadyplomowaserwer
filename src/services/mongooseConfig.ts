import mongoose from 'mongoose';

import { MONGO_URI } from '../../config/keys';

(async () => {
  await mongoose.connect(MONGO_URI, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  mongoose.connection.on('connected', () => {
    console.log('Connected to MongoDB');
  });

  mongoose.connection.on('error', err => {
    console.error(err);
  });
})();
