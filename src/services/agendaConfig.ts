import Agenda from 'agenda';

import { MONGO_URI } from '../../config/keys';
import sendEmails from '../utils/sendEmails';

const agenda = new Agenda({
  db: { address: MONGO_URI, collection: 'jobCollection' },
});

async function closeAgenda() {
  await agenda.stop();
  process.exit(0);
}

process.on('SIGTERM', closeAgenda);
process.on('SIGINT', closeAgenda);

(async function() {
  await agenda.start();
  agenda.define('send-emails', sendEmails);
  const sendEmailsJob = agenda.create('send-emails');
  sendEmailsJob.repeatAt('6:00am');
  await sendEmailsJob.save();
})();
