import express from 'express';

import { AUTH_ROUTES } from '../consts';
import * as AuthCtrl from '../controllers/auth';

const router = express.Router();

router.post(`/${AUTH_ROUTES.SIGN_IN}`, AuthCtrl.signin);
router.post(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`, AuthCtrl.signupEmail);
router.post(`/${AUTH_ROUTES.SIGN_UP_GOOGLE}`, AuthCtrl.signupGoogle);

export default router;
