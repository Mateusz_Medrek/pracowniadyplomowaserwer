import express from 'express';

import { USER_ROUTES } from '../consts';
import * as UserCtrl from '../controllers/user';
import requireAuth from '../middlewares/requireAuth';

const router = express.Router();

router.post(`/${USER_ROUTES.ADD_FRIEND}`, requireAuth, UserCtrl.addFriend);
router.post(`/${USER_ROUTES.CHANGE_EMAIL}`, requireAuth, UserCtrl.changeEmail);
router.post(`/${USER_ROUTES.CHANGE_NAME}`, requireAuth, UserCtrl.changeName);
router.post(
  `/${USER_ROUTES.CHANGE_PASSWORD}`,
  requireAuth,
  UserCtrl.changePassword
);
router.get(
  `/${USER_ROUTES.GET_CURRENT_USER}`,
  requireAuth,
  UserCtrl.getCurrentUser
);
router.post(
  `/${USER_ROUTES.REMOVE_FRIEND}`,
  requireAuth,
  UserCtrl.removeFriend
);
router.get(`/${USER_ROUTES.GET_USER}/:id`, requireAuth, UserCtrl.getUser);
router.get(
  `/${USER_ROUTES.GET_USER_BY_NAME}`,
  requireAuth,
  UserCtrl.getUserByName
);

export default router;
