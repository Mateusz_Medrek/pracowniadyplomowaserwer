import express from 'express';

import { TASK_ROUTES } from '../consts';
import * as TaskCtrl from '../controllers/task';
import requireAuth from '../middlewares/requireAuth';

const router = express.Router();

router.post(`/${TASK_ROUTES.CHECK_TASK}`, requireAuth, TaskCtrl.checkTask);
router.post(
  `/${TASK_ROUTES.CHECK_SUBTASK}`,
  requireAuth,
  TaskCtrl.checkSubtask
);
router.post(`/${TASK_ROUTES.CREATE_TASK}`, requireAuth, TaskCtrl.createTask);
router.post(`/${TASK_ROUTES.DELETE_TASK}`, requireAuth, TaskCtrl.deleteTask);
router.post(`/${TASK_ROUTES.EDIT_TASK}`, requireAuth, TaskCtrl.editTask);
router.get(
  `/${TASK_ROUTES.GET_ALL_COMPLETED_TASKS}`,
  requireAuth,
  TaskCtrl.getAllCompletedTasks
);
router.get(`/${TASK_ROUTES.GET_ALL_TASKS}`, requireAuth, TaskCtrl.getAllTasks);
router.get(
  `/${TASK_ROUTES.GET_ALL_TODAY_TASKS}`,
  requireAuth,
  TaskCtrl.getAllTodayTasks
);
router.get(`/${TASK_ROUTES.GET_TASK}/:id`, requireAuth, TaskCtrl.getTask);
router.post(`/${TASK_ROUTES.UNCHECK_TASK}`, requireAuth, TaskCtrl.uncheckTask);
router.post(
  `/${TASK_ROUTES.UNCHECK_SUBTASK}`,
  requireAuth,
  TaskCtrl.uncheckSubtask
);

export default router;
