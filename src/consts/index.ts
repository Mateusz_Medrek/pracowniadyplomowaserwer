export const EMAIL_REGEX = new RegExp(
  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
);

export const AUTH_ROUTES = {
  SIGN_IN: 'auth/signin',
  SIGN_UP_EMAIL: 'auth/signupemail',
  SIGN_UP_FACEBOOK: 'auth/signupfacebook',
  SIGN_UP_GOOGLE: 'auth/signupgoogle',
};

export const TASK_ROUTES = {
  CHECK_TASK: 'task/check',
  CREATE_TASK: 'task/create',
  DELETE_TASK: 'task/delete',
  EDIT_TASK: 'task/edit',
  GET_ALL_COMPLETED_TASKS: 'task/getallcompleted',
  GET_ALL_TASKS: 'task/getall',
  GET_ALL_TODAY_TASKS: 'task/getalltoday',
  GET_TASK: 'task/get',
  UNCHECK_TASK: 'task/uncheck',
  CHECK_SUBTASK: 'subtask/check',
  UNCHECK_SUBTASK: 'subtask/uncheck',
};

export const USER_ROUTES = {
  CHANGE_EMAIL: 'user/emailchange',
  CHANGE_NAME: 'user/namechange',
  CHANGE_PASSWORD: 'user/passwordchange',
  GET_CURRENT_USER: 'user/current',
  GET_USER: 'user/get',
  GET_USER_BY_NAME: 'user/getbyname',
  ADD_FRIEND: 'friend/add',
  REMOVE_FRIEND: 'friend/remove',
};
