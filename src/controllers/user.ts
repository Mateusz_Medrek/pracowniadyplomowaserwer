import { RequestHandler } from 'express-serve-static-core';
import { model } from 'mongoose';

import { UserModel } from '../models/types';
import getUserModelId from '../utils/getUserModelId';

type AddFriendBody = { id: string };
type ChangeEmailBody = { email: string; password: string };
type ChangeNameBody = { name: string };
type ChangePasswordBody = { oldPassword: string; newPassword: string };
type GetUserParams = { id: string };
type FriendIdAndName = { id: string; name: string };
type GetUserByNameQuery = { name: string };

const User = model<UserModel>('User');

const gettingUser = async (id: string) => {
  const user = await User.findOne({ id });
  if (!user) {
    throw new Error('User not found');
  }
  const friends: FriendIdAndName[] = [];
  for (const friendId of user.friends.keys()) {
    const friend = (await User.find({ id: friendId }))[0];
    if (friend) {
      friends.push({ id: friendId, name: friend.name });
    }
  }
  return {
    id: user.id,
    name: user.name,
    email: user.email,
    accountType: user.accountType,
    tasksIds: user.tasksIds.map(taskId => taskId.toHexString()),
    friends,
  };
};

export const addFriend: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  const { id }: AddFriendBody = req.body;
  const tokenWithProvider = authorization!.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  const userId = await getUserModelId(provider, token);
  if (!userId) {
    return res.status(400).send();
  }
  try {
    const user = await User.findOne({ id: userId });
    if (!user) {
      throw new Error('Adding friend failure');
    }
    user.friends.set(id, true);
    const friend = (await User.find({ id }))[0];
    friend.friends.set(userId, true);
    await Promise.all([friend.save(), user.save()]);
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const changeEmail: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  const { email, password }: ChangeEmailBody = req.body;
  if (!email) {
    return res.status(400).send('Email is not provided');
  }
  if (!password) {
    return res.status(400).send('Password is not provided');
  }
  const tokenWithProvider = authorization!.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  const userId = await getUserModelId(provider, token);
  if (!userId) {
    return res.status(400).send();
  }
  try {
    const user = (await User.findOne({ id: userId }))!;
    const result = await user.comparePassword(password);
    if (!result) {
      throw new Error('Password is incorrect');
    }
    user.email = email;
    await user.save();
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const changeName: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  const { name }: ChangeNameBody = req.body;
  if (!name) {
    return res.status(400).send('Name is not provided');
  }
  const tokenWithProvider = authorization!.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  const userId = await getUserModelId(provider, token);
  if (!userId) {
    return res.status(400).send();
  }
  try {
    await User.findOneAndUpdate({ id: userId }, { name });
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const changePassword: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  const { oldPassword, newPassword }: ChangePasswordBody = req.body;
  if (!oldPassword) {
    return res.status(400).send('Old password is not provided');
  }
  if (!newPassword) {
    return res.status(400).send('New password is not provided');
  }
  const tokenWithProvider = authorization!.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  const userId = await getUserModelId(provider, token);
  if (!userId) {
    return res.status(400).send();
  }
  try {
    const user = (await User.findOne({ id: userId }))!;
    const result = await user.comparePassword(oldPassword);
    if (!result) {
      throw new Error('Password is incorrect');
    }
    user.password = newPassword;
    await user.save();
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const getCurrentUser: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  const tokenWithProvider = authorization!.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  const id = await getUserModelId(provider, token);
  if (!id) {
    return res.status(400).send();
  }
  try {
    const currentUser = await gettingUser(id);
    return res.status(200).send(currentUser);
  } catch (err) {
    return res.status(400).send({ error: err.message });
  }
};

export const getUser: RequestHandler<GetUserParams> = async (req, res) => {
  const { id } = req.params;
  try {
    const currentUser = await gettingUser(id);
    return res.status(200).send(currentUser);
  } catch (err) {
    return res.status(404).send({ error: err.message });
  }
};

export const getUserByName: RequestHandler = async (req, res) => {
  const { name }: GetUserByNameQuery = req.query;
  if (!name) {
    return res.status(400).send({ error: 'No name parameter provided' });
  }
  const users =
    (await User.find({
      name: new RegExp('^' + name.toLowerCase() + '$', 'i'),
    })) || [];
  return res.status(200).send({
    users: users.map(user => ({
      id: user.id,
      name: user.name,
    })),
  });
};

export const removeFriend: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  const { id }: AddFriendBody = req.body;
  const tokenWithProvider = authorization!.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  const userId = await getUserModelId(provider, token);
  if (!userId) {
    return res.status(400).send();
  }
  try {
    const user = await User.findOne({ id: userId });
    if (!user) {
      throw new Error('Removing friend failure');
    }
    user.friends.set(id, undefined as any);
    const friend = (await User.find({ id }))[0];
    friend.friends.set(userId, undefined as any);
    await Promise.all([friend.save(), user.save()]);
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};
