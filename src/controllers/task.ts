import { RequestHandler } from 'express-serve-static-core';
import { model } from 'mongoose';

import { TaskModel, UserModel, SubTask } from '../models/types';
import getUserModelId from '../utils/getUserModelId';

enum GettingAllTasks {
  ALL = 0,
  ONLY_COMPLETED = 1,
  ONLY_TODAY = 2,
}

type CheckTaskBody = { id: string };
type CheckSubtaskBody = { id: string; subTaskId: number };
type CreateTaskBody = Pick<
  TaskModel,
  'title' | 'description' | 'startingTime' | 'finishingTime' | 'subTasks'
>;
type DeleteTaskBody = { id: string };
type EditTaskBody = Pick<
  TaskModel,
  'title' | 'description' | 'startingTime' | 'finishingTime' | 'subTasks'
> & { id: string };
type GetTaskParams = { id: string };
type TasksIdAndTitle = { id: string; title: string };

const Task = model<TaskModel>('Task');
const User = model<UserModel>('User');

const checkingTask = async (
  id: string,
  isCompleted: boolean
): Promise<void> => {
  try {
    const task = await Task.findOneAndUpdate({ _id: id }, { isCompleted });
    if (!task) {
      throw new Error('There is no task in database with given id');
    }
    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
};

const checkingSubtasks = async (
  id: string,
  isCompleted: boolean,
  subTaskId: number
): Promise<void> => {
  try {
    const task = await Task.findOne({ _id: id });
    if (!task) {
      throw new Error('There is no task in database with given id');
    }
    task.subTasks = task.subTasks.map(subTask => {
      if (subTask.id === subTaskId) {
        subTask.isCompleted = isCompleted;
      }
      return subTask;
    });
    await task.save();
    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
};

const gettingAllTasks = async (
  variant: GettingAllTasks,
  authorization?: string
): Promise<TasksIdAndTitle[]> => {
  const tokenWithProvider = authorization!.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  const userId = await getUserModelId(provider, token);
  if (!userId) {
    return Promise.reject(new Error('Bad token'));
  }
  try {
    const user = await User.findOne({ id: userId });
    if (!user) {
      throw new Error('There is no user in database with given id');
    }
    const isCompleted = variant === GettingAllTasks.ONLY_COMPLETED;
    const today = new Date();
    const timeCondition =
      variant === GettingAllTasks.ONLY_TODAY
        ? {
            startingTime: { $gte: today },
            finishingTime: { $lte: today },
          }
        : {};
    const tasks = (
      await Task.find({
        _id: { $in: user.tasksIds },
        isCompleted,
        ...timeCondition,
      })
    ).map<TasksIdAndTitle>(task => ({
      id: task._id,
      title: task.title,
    }));
    return Promise.resolve(tasks);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const checkTask: RequestHandler = async (req, res) => {
  const { id }: CheckTaskBody = req.body;
  try {
    await checkingTask(id, true);
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const checkSubtask: RequestHandler = async (req, res) => {
  const { id, subTaskId }: CheckSubtaskBody = req.body;
  if (!subTaskId) {
    return res.status(400).send('There is no subtask in provided task');
  }
  try {
    await checkingSubtasks(id, true, subTaskId);
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const createTask: RequestHandler = async (req, res) => {
  const {
    title,
    description,
    startingTime,
    finishingTime,
    subTasks,
  }: CreateTaskBody = req.body;
  if (!title || !description || !startingTime || !finishingTime) {
    return res.status(400).send({ error: 'Arguments missing' });
  }
  const task = new Task({
    title,
    description,
    startingTime,
    finishingTime,
  });
  for (let i = 0; i < subTasks.length; i++) {
    try {
      task.subTasks.push(subTasks[i]);
      const user = await User.findOne({
        id: subTasks[i].assigneeId,
        tasksIds: { $ne: task._id },
      });
      if (user) {
        user.tasksIds.push(task._id);
        await user.save();
      }
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }
  await task.save();
  return res.status(200).send();
};

export const deleteTask: RequestHandler = async (req, res) => {
  const { id }: DeleteTaskBody = req.body;
  try {
    const task = await Task.findOneAndDelete({ _id: id });
    if (!task) {
      throw new Error('There is no task in database with given id');
    }
    const users = await User.find({ tasksIds: task._id });
    for (let i = 0; i < users.length; i++) {
      users[i].tasksIds = users[i].tasksIds.filter(
        taskId => taskId.toHexString() !== task._id.toHexString()
      );
      await users[i].save();
    }
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const editTask: RequestHandler = async (req, res) => {
  const {
    id,
    title,
    description,
    startingTime,
    finishingTime,
    subTasks,
  }: EditTaskBody = req.body;
  try {
    const task = await Task.findOne({ _id: id });
    if (!task) {
      throw new Error('There is no task in database with given id');
    }
    !!title && (task.title = title);
    !!description && (task.description = description);
    !!startingTime && (task.startingTime = startingTime);
    !!finishingTime && (task.finishingTime = finishingTime);
    !!subTasks && (task.subTasks = subTasks);
    await task.save();
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const getAllCompletedTasks: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  try {
    const tasks = await gettingAllTasks(
      GettingAllTasks.ONLY_COMPLETED,
      authorization
    );
    return res.status(200).send({ tasks });
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const getAllTasks: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  try {
    const tasks = await gettingAllTasks(GettingAllTasks.ALL, authorization);
    return res.status(200).send({ tasks });
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const getAllTodayTasks: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  try {
    const tasks = await gettingAllTasks(
      GettingAllTasks.ONLY_TODAY,
      authorization
    );
    return res.status(200).send({ tasks });
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const getTask: RequestHandler<GetTaskParams> = async (req, res) => {
  const { id } = req.params;
  try {
    const task = await Task.findOne({ _id: id });
    if (!task) {
      throw new Error('There is no task in database with given id');
    }
    return res.status(200).send({
      title: task.title,
      description: task.description,
      startingTime: task.startingTime,
      finishingTime: task.finishingTime,
      subTasks: task.subTasks.map<SubTask>(subTask => ({
        assigneeId: subTask.assigneeId,
        category: subTask.category,
        id: subTask.id,
        isCompleted: subTask.isCompleted,
        title: subTask.title,
      })),
      isCompleted: task.isCompleted,
    });
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const uncheckTask: RequestHandler = async (req, res) => {
  const { id }: CheckTaskBody = req.body;
  try {
    await checkingTask(id, false);
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};

export const uncheckSubtask: RequestHandler = async (req, res) => {
  const { id, subTaskId }: CheckSubtaskBody = req.body;
  if (!subTaskId) {
    return res.status(400).send('There is no subtask in provided task');
  }
  try {
    await checkingSubtasks(id, false, subTaskId);
    return res.status(200).send();
  } catch (err) {
    return res.status(400).send(err.message);
  }
};
