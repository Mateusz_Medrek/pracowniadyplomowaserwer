import { RequestHandler } from 'express-serve-static-core';
import { OAuth2Client } from 'google-auth-library';
import { model } from 'mongoose';
import jwt from 'jsonwebtoken';

import { GOOGLE_CLIENT_ID, JWT_SECRET_KEY } from '../../config/keys';
import { Providers, UserModel } from '../models/types';
import { EMAIL_REGEX } from '../consts';

type SigninBody = { email: string; password: string };
type SignupEmailBody = { email: string; password: string; name: string };

const User = model<UserModel>('User');

export const signin: RequestHandler = async (req, res) => {
  const { email, password }: SigninBody = req.body;
  if (!email || !password) {
    return res
      .status(401)
      .send({ error: 'Email and password must be provided' });
  }
  if (!email.match(EMAIL_REGEX)) {
    return res.status(401).send({ error: 'Email has invalid format' });
  }
  const user = await User.findOne({ email, accountType: Providers.email });
  if (!user) {
    return res.status(401).send({ error: 'Invalid email' });
  }
  try {
    await user.comparePassword(password);
    const token = jwt.sign({ userId: user.id }, JWT_SECRET_KEY);
    return res.send(token);
  } catch (err) {
    return res.status(401).send({ error: 'Invalid password' });
  }
};

export const signupEmail: RequestHandler = async (req, res) => {
  const { email, password, name }: SignupEmailBody = req.body;
  if (!email || !password || !name) {
    return res
      .status(401)
      .send({ error: 'Email, password and name must be provided' });
  }
  if (!email.match(EMAIL_REGEX)) {
    return res.status(401).send({ error: 'Email has invalid format' });
  }
  try {
    const isThereIsAlreadyUserWithThisEmail = await User.findOne({
      email,
      accountType: Providers.email,
    });
    if (isThereIsAlreadyUserWithThisEmail) {
      return res
        .status(401)
        .send({ error: 'There is already account with this email' });
    }
    const user = new User({
      accountType: Providers.email,
      email,
      name,
    });
    user.friends = new Map<string, boolean>();
    user.id = Providers.email + user._id.toHexString();
    user.tasksIds = [];
    user.password = password;
    await user.save();
    const token = jwt.sign({ userId: user.id }, JWT_SECRET_KEY);
    return res.send(token);
  } catch (error) {
    return res.status(401).send({ error });
  }
};

export const signupGoogle: RequestHandler = async (req, res) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).send({ error: 'Not authenticated' });
  }
  const token = authorization.replace('Bearer ', '');
  try {
    const oauth2Client = new OAuth2Client({
      clientId: GOOGLE_CLIENT_ID,
    });
    const ticket = await oauth2Client.verifyIdToken({
      idToken: token,
      audience: GOOGLE_CLIENT_ID,
    });
    const payload = ticket.getPayload();
    if (
      !payload ||
      payload.aud !== GOOGLE_CLIENT_ID ||
      !(
        payload.iss === 'https://accounts.google.com' ||
        payload.iss === 'accounts.google.com'
      ) ||
      !payload.exp ||
      payload.exp <= 0
    ) {
      return res.status(401).send({ error: 'Not authenticated' });
    }
    const user = await User.findOne({ id: Providers.google + payload.sub });
    if (!user) {
      const newUser = new User({
        accountType: Providers.google,
        email: payload.email,
        name: payload.name,
      });
      newUser.friends = new Map<string, boolean>();
      newUser.id = Providers.google + payload.sub;
      newUser.tasksIds = [];
      await newUser.save();
    }
    return res.send(token);
  } catch (err) {
    return res.status(401).send(err.message);
  }
};
