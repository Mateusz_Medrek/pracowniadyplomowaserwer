import './models/User';
import './models/Task';

import './services/mongooseConfig';
import './services/agendaConfig';

import app from './services/appConfig';
import authRoutes from './routes/auth';
import taskRoutes from './routes/task';
import userRoutes from './routes/user';

app.use(authRoutes);
app.use(taskRoutes);
app.use(userRoutes);

app.get('/', (req, res) => {
  res.send('Hi, I am initial dummy endpoint');
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
