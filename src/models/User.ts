import bcrypt from 'bcrypt';
import { model, Schema } from 'mongoose';

import { UserModel } from './types';

const userSchema = new Schema<UserModel>({
  name: String,
  accountType: String,
  id: {
    type: String,
    unique: true,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  /**
   *  Password is not required,
   *  because it will be used only
   *  for email provided accounts;
   *
   *  id is unique, first character
   *  is provider, the rest:
   *    for email account it is mongo _id;
   *    for FB account it is unique FB user id;
   *    for Google account it is unique Google user id;
   */
  password: {
    type: String,
    required: false,
  },
  tasksIds: [{ type: Schema.Types.ObjectId, ref: 'Task' }],
  friends: {
    type: Map,
    of: Boolean,
  },
});

userSchema.pre<UserModel>('save', function(next) {
  const user = this;
  if (!user.isModified('password')) {
    return next();
  }

  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }

    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function(
  candidatePassword: string
): Promise<boolean | Error> {
  const user = this;
  return new Promise((resolve, reject) => {
    bcrypt.compare(candidatePassword, user.password!, (err, isPwdCorrect) => {
      if (err) {
        return reject(err);
      }
      if (!isPwdCorrect) {
        return reject(false);
      }
      return resolve(true);
    });
  });
};

model<UserModel>('User', userSchema);
