import { Document, Types } from 'mongoose';

export enum Providers {
  email = '0',
  facebook = '1',
  google = '2',
}

export interface SubTask {
  assigneeId: string;
  category: number;
  isCompleted?: boolean;
  id: number;
  title: string;
}

export interface TaskModel extends Document {
  title: string;
  description: string;
  startingTime: Date;
  finishingTime: Date;
  subTasks: SubTask[];
  isCompleted?: boolean;
}

export interface UserModel extends Document {
  accountType: Providers.email | Providers.facebook | Providers.google;
  name: string;
  id: string;
  email: string;
  password?: string;
  tasksIds: Types.ObjectId[];
  friends: Map<string, boolean>;
  comparePassword: (candidatePassword: string) => Promise<boolean | Error>;
}
