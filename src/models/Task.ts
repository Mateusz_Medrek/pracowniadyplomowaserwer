import { model, Schema } from 'mongoose';

import { TaskModel } from './types';

const taskSchema = new Schema<TaskModel>({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  startingTime: {
    type: Schema.Types.Date,
    required: true,
  },
  finishingTime: {
    type: Schema.Types.Date,
    required: true,
  },
  subTasks: [
    {
      assigneeId: { type: String, required: true },
      category: { type: Number, required: true },
      id: { type: Number, required: true },
      isCompleted: { type: Boolean, default: false, required: false },
      title: { type: String, required: true },
    },
  ],
  isCompleted: {
    type: Boolean,
    default: false,
    required: false,
  },
});

model<TaskModel>('Task', taskSchema);
