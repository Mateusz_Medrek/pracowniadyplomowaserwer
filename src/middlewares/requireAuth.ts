import { NextFunction, Request, Response } from 'express';
import { OAuth2Client } from 'google-auth-library';
import jwt from 'jsonwebtoken';

import { GOOGLE_CLIENT_ID, JWT_SECRET_KEY } from '../../config/keys';

import { Providers } from '../models/types';

const requireAuth = async (req: Request, res: Response, next: NextFunction) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).send({ error: 'Not authenticated' });
  }
  const tokenWithProvider = authorization.replace('Bearer ', '');
  const provider = tokenWithProvider[0];
  const token = tokenWithProvider.slice(1);
  if (provider === Providers.email) {
    return jwt.verify(token, JWT_SECRET_KEY, async (err, payload) => {
      if (err) {
        return res.status(401).send({ error: err });
      }
      const { userId }: { userId: string } = payload as any;
      if (!userId) {
        return res.status(401).send({ error: 'Not authenticated' });
      }
      return next();
    });
  }
  if (provider === Providers.google) {
    const oauth2Client = new OAuth2Client({
      clientId: GOOGLE_CLIENT_ID,
    });
    try {
      const loginTicket = await oauth2Client.verifyIdToken({
        idToken: token,
        audience: GOOGLE_CLIENT_ID,
      });
      const payload = loginTicket.getPayload();
      if (
        !payload ||
        payload.aud !== GOOGLE_CLIENT_ID ||
        !(
          payload.iss === 'https://accounts.google.com' ||
          payload.iss === 'accounts.google.com'
        ) ||
        !payload.exp ||
        payload.exp <= 0
      ) {
        return res.status(401).send({ error: 'Not authenticated' });
      }
      return next();
    } catch (err) {
      return res.status(401).send({ error: 'Not authenticated' });
    }
  }
  return res.status(401).send({ error: 'No provider' });
};

export default requireAuth;
