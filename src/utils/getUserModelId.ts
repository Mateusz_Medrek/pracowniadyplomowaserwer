import { OAuth2Client } from 'google-auth-library';
import jwt from 'jsonwebtoken';

import { GOOGLE_CLIENT_ID, JWT_SECRET_KEY } from '../../config/keys';
import { Providers } from '../models/types';

const getUserModelId = async (provider: string, tkn: string) => {
  if (provider === Providers.email) {
    return new Promise<string>((resolve, reject) => {
      jwt.verify(tkn, JWT_SECRET_KEY, async (err, payload) => {
        if (err) {
          return reject('');
        }
        const { userId }: { userId: string } = payload as any;
        if (!userId) {
          return reject('');
        }
        return resolve(userId);
      });
    });
  }
  if (provider === Providers.google) {
    try {
      const oauth2Client = new OAuth2Client({
        clientId: GOOGLE_CLIENT_ID,
      });
      const ticket = await oauth2Client.verifyIdToken({
        idToken: tkn,
        audience: GOOGLE_CLIENT_ID,
      });
      const payload = ticket.getPayload();
      if (payload) {
        return Providers.google + payload.sub;
      }
      throw new Error();
    } catch (err) {
      return Promise.reject(err);
    }
  }
  return Promise.reject('No provider');
};

export default getUserModelId;
