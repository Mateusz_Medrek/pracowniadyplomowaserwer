import { model } from 'mongoose';

import { NODEMAILER_USERNAME } from '../../config/keys';
import { TaskModel, UserModel } from '../models/types';
import transporter from '../services/nodemailerConfig';

const sendEmails = async function() {
  const Task = model<TaskModel>('Task');
  const User = model<UserModel>('User');
  const users = await User.find();
  await Promise.all(
    users.map(async user => {
      const tasks: TaskModel[] = [];
      await Promise.all(
        user.tasksIds.map(async taskId => {
          const task = await Task.findOne({ _id: taskId });
          if (!task || task.isCompleted) {
            return;
          }
          const currentDateTime = new Date();
          if (currentDateTime >= task.startingTime) {
            tasks.push(task);
          }
        })
      );
      if (!tasks.length) {
        return;
      }
      await transporter.sendMail({
        from: `"ToDoApp" ${NODEMAILER_USERNAME}`,
        to: `${user.email}`,
        subject: `You have ${tasks.length} today`,
        html:
          '<h3>Check your to-do list and have a nice day!</h3>' +
          '<h4>Your tasks are:</h4>' +
          '<ul>' +
          tasks.map(task => `<li>${task.title}</li>`) +
          '</ul>',
      });
    })
  );
};

export default sendEmails;
