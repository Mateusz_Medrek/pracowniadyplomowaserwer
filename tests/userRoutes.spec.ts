jest.mock('google-auth-library');

import '../src/models/User';

import { Request, Response, NextFunction } from 'express';
import { OAuth2Client } from 'google-auth-library';
import mockingoose from 'mockingoose';
import { model } from 'mongoose';
import request from 'supertest';

import { USER_ROUTES } from '../src/consts';
import { Providers, UserModel } from '../src/models/types';
import userRoutes from '../src/routes/user';
import app from '../src/services/appConfig';

const User = model<UserModel>('User');

const FRIEND_EMAIL = 'friendEmail@gmail.com';
const FRIEND_ID = '012345';
const FRIEND_NAME = 'XYZ';
const ID = '0skdjvbs21984bisdbvi9232f';
const NAME = 'ABCDEF';
const NEW_PASSWORD = 'newPassword';
const SOME_EMAIL = 'someEmail@gmail.com';
const MOCK_VALID_EMAIL_TOKEN = 'Bearer 0IAmValidEmailToken';
const MOCK_VALID_GOOGLE_TOKEN = 'Bearer 2IAmValidGoogleToken';
const MOCK_VALID_PASSWORD = 'validPassword';

jest.mock('../src/middlewares/requireAuth', () =>
  jest.fn((req: Request, res: Response, next: NextFunction) => {
    if (
      [MOCK_VALID_EMAIL_TOKEN, MOCK_VALID_GOOGLE_TOKEN].includes(
        req.headers.authorization ?? ''
      )
    ) {
      return next();
    } else {
      return res.status(401).send({ error: 'Not authenticated' });
    }
  })
);

jest.mock('jsonwebtoken', () => ({
  sign: jest.fn(() => {
    return 'IAmValidEmailToken';
  }),
  verify: jest.fn((token, secret, callback) => {
    if (token === 'IAmValidEmailToken') {
      callback(undefined, { userId: '0skdjvbs21984bisdbvi9232f' });
    } else {
      callback(undefined, {});
    }
  }),
}));

jest.mock('bcrypt', () => ({
  compare: jest.fn(
    (
      candidate: string,
      realPassword: string,
      callback: (err: Error | undefined, same: boolean) => void
    ) => {
      if (candidate !== MOCK_VALID_PASSWORD) {
        callback(undefined, false);
      } else {
        callback(undefined, true);
      }
    }
  ),
  genSalt: jest.fn((rounds, callback) => {
    callback(undefined, '98765');
  }),
  hash: jest.fn((data, salt, callback) => {
    callback(undefined, MOCK_VALID_PASSWORD);
  }),
}));

const verifyIdToken = jest.fn(
  ({ idToken, audience }: { idToken: string; audience: string }) => {
    if (idToken === 'IAmValidGoogleToken') {
      return {
        getPayload: jest.fn(() => ({
          aud: audience,
          exp: 123,
          iss: 'accounts.google.com',
          sub: 'Google0123456789',
        })),
      };
    } else {
      return {
        getPayload: jest.fn(() => undefined),
      };
    }
  }
);

const FRIEND = {
  id: FRIEND_ID,
  name: FRIEND_NAME,
};

app.use(userRoutes);

describe('userRoutes', () => {
  describe(`/${USER_ROUTES.ADD_FRIEND}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error after User update failure', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      const response = await request(app)
        .post(`/${USER_ROUTES.ADD_FRIEND}`)
        .send(FRIEND)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Adding friend failure');
    });
    it('should return OK after successful User update', async () => {
      const friend: [string, boolean] = [FRIEND.id, true];
      const _doc = {
        id: ID,
        name: NAME,
        email: SOME_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>([friend]),
      };
      const _friendDoc = {
        id: FRIEND_ID,
        name: FRIEND_NAME,
        email: FRIEND_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>(),
      };
      mockingoose(User).toReturn(_doc, 'findOne');
      mockingoose(User).toReturn([_friendDoc], 'find');
      _doc.friends.set(FRIEND.id, true);
      mockingoose(User).toReturn(_doc, 'save');
      const response = await request(app)
        .post(`/${USER_ROUTES.ADD_FRIEND}`)
        .send(FRIEND)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${USER_ROUTES.CHANGE_EMAIL}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when email is not provided (or is empty)', async () => {
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_EMAIL}`)
        .send({ password: MOCK_VALID_PASSWORD })
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Email is not provided');
    });
    it('should return error when password is not provided', async () => {
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_EMAIL}`)
        .send({ email: SOME_EMAIL })
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Password is not provided');
    });
    it('should return OK after successful email update', async () => {
      const _doc = {
        id: ID,
        name: NAME,
        email: 'mn' + SOME_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>(),
      };
      mockingoose(User).toReturn(_doc, 'findOne');
      _doc.email = SOME_EMAIL;
      mockingoose(User).toReturn(_doc, 'save');
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_EMAIL}`)
        .send({ email: SOME_EMAIL, password: MOCK_VALID_PASSWORD })
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${USER_ROUTES.CHANGE_NAME}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when name is not provided (or is empty)', async () => {
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_NAME}`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Name is not provided');
    });
    it('should return OK after successful name update', async () => {
      const _doc = {
        id: ID,
        name: NAME,
        email: SOME_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>(),
      };
      mockingoose(User).toReturn(_doc, 'findOneAndUpdate');
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_NAME}`)
        .send({ name: NAME })
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${USER_ROUTES.CHANGE_PASSWORD}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when old password is not provided (or is empty)', async () => {
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_PASSWORD}`)
        .send({ newPassword: NEW_PASSWORD })
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Old password is not provided');
    });
    it('should return error when new password is not provided (or is empty)', async () => {
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_PASSWORD}`)
        .send({ oldPassword: MOCK_VALID_PASSWORD })
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('New password is not provided');
    });
    it('should return OK after successful password update', async () => {
      const _doc: any = {
        id: ID,
        name: NAME,
        email: SOME_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>(),
      };
      mockingoose(User).toReturn(_doc, 'findOne');
      _doc.password = NEW_PASSWORD;
      mockingoose(User).toReturn(_doc, 'save');
      const response = await request(app)
        .post(`/${USER_ROUTES.CHANGE_PASSWORD}`)
        .send({ oldPassword: MOCK_VALID_PASSWORD, newPassword: NEW_PASSWORD })
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${USER_ROUTES.REMOVE_FRIEND}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error after User update failure', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      const response = await request(app)
        .post(`/${USER_ROUTES.REMOVE_FRIEND}`)
        .send(FRIEND)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Removing friend failure');
    });
    it('should return OK after successful User update', async () => {
      const friend: [string, boolean] = [FRIEND.id, true];
      const _doc = {
        id: ID,
        name: NAME,
        email: SOME_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>([
          friend,
          [FRIEND.id, true] as [string, boolean],
        ]),
      };
      const _friendDoc = {
        id: FRIEND_ID,
        name: FRIEND_NAME,
        email: FRIEND_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>([[ID, true] as [string, boolean]]),
      };
      mockingoose(User).toReturn(_doc, 'findOne');
      mockingoose(User).toReturn([_friendDoc], 'find');
      _doc.friends.set(FRIEND.id, undefined as any);
      mockingoose(User).toReturn(_doc, 'save');
      const response = await request(app)
        .post(`/${USER_ROUTES.REMOVE_FRIEND}`)
        .send(FRIEND)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${USER_ROUTES.GET_USER}/:id`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error 404 when there is no user in database with given id', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      const response = await request(app)
        .get(`/${USER_ROUTES.GET_USER}/${ID}`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(404);
      expect(response.body.error).toEqual('User not found');
    });
    it('should return user with given id', async () => {
      const friend: [string, boolean] = [FRIEND.id, true];
      const user = {
        id: ID,
        name: NAME,
        email: SOME_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>([friend]),
      };
      mockingoose(User).toReturn(user, 'findOne');
      mockingoose(User).toReturn([FRIEND], 'find');
      const response = await request(app)
        .get(`/${USER_ROUTES.GET_USER}/${ID}`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.id).toEqual(ID);
      expect(response.body.name).toEqual(user.name);
      expect(response.body.email).toEqual(user.email);
      expect(response.body.accountType).toEqual(user.accountType);
      expect(response.body.tasksIds).toEqual(user.tasksIds);
      expect(response.body.friends[0].id).toEqual(FRIEND.id);
      expect(response.body.friends[0].name).toEqual(FRIEND.name);
    });
  });
  describe(`/${USER_ROUTES.GET_USER_BY_NAME}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no name parameter provided or it is empty string', async () => {
      let response = await request(app)
        .get(`/${USER_ROUTES.GET_USER_BY_NAME}`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.body.error).toEqual('No name parameter provided');
      response = await request(app)
        .get(`/${USER_ROUTES.GET_USER_BY_NAME}`)
        .query(`name=`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.body.error).toEqual('No name parameter provided');
    });
    it('should return empty array when there is no user in database with given name', async () => {
      mockingoose(User).toReturn(undefined, 'find');
      const response = await request(app)
        .get(`/${USER_ROUTES.GET_USER_BY_NAME}`)
        .query(`name=${NAME}`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.users).toHaveLength(0);
    });
    it('should return array of users when there are users in database with given name', async () => {
      const _doc1 = {
        id: ID,
        accountType: Providers.email,
        name: NAME,
        email: SOME_EMAIL,
      };
      const _doc2 = {
        id: FRIEND_ID,
        accountType: Providers.email,
        name: NAME,
        email: FRIEND_EMAIL,
      };
      mockingoose(User).toReturn(() => {
        return [_doc1, _doc2];
      }, 'find');
      const response = await request(app)
        .get(`/${USER_ROUTES.GET_USER_BY_NAME}`)
        .query(`name=${NAME}`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.users).toHaveLength(2);
      expect(response.body.users[0].id).toEqual(_doc1.id);
      expect(response.body.users[0].name).toEqual(_doc1.name);
      expect(response.body.users[1].id).toEqual(_doc2.id);
      expect(response.body.users[1].name).toEqual(_doc2.name);
    });
  });
  describe(`/${USER_ROUTES.GET_CURRENT_USER}`, () => {
    beforeAll(() => {
      (OAuth2Client as any).mockImplementation(() => ({
        verifyIdToken,
      }));
    });
    beforeEach(() => {
      (OAuth2Client as any).mockClear();
      verifyIdToken.mockClear();
      mockingoose.resetAll();
    });
    it('should return user object', async () => {
      const friend: [string, boolean] = [FRIEND.id, true];
      const _doc = {
        id: ID,
        name: NAME,
        email: SOME_EMAIL,
        accountType: Providers.email,
        tasksIds: [],
        friends: new Map<string, boolean>([friend]),
      };
      mockingoose(User).toReturn(_doc, 'findOne');
      mockingoose(User).toReturn([FRIEND], 'find');
      let response = await request(app)
        .get(`/${USER_ROUTES.GET_CURRENT_USER}`)
        .set('Authorization', MOCK_VALID_EMAIL_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.id).toEqual(ID);
      expect(response.body.name).toEqual(NAME);
      expect(response.body.email).toEqual(SOME_EMAIL);
      expect(response.body.accountType).toEqual(_doc.accountType);
      expect(response.body.tasksIds).toEqual(_doc.tasksIds);
      expect(response.body.friends[0].id).toEqual(FRIEND.id);
      expect(response.body.friends[0].name).toEqual(FRIEND.name);
      mockingoose(User).reset('findOne');
      _doc.accountType = Providers.google;
      mockingoose(User).toReturn(_doc, 'findOne');
      response = await request(app)
        .get(`/${USER_ROUTES.GET_CURRENT_USER}`)
        .set('Authorization', MOCK_VALID_GOOGLE_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.id).toEqual(ID);
      expect(response.body.name).toEqual(NAME);
      expect(response.body.email).toEqual(SOME_EMAIL);
      expect(response.body.accountType).toEqual(_doc.accountType);
      expect(response.body.tasksIds).toEqual(_doc.tasksIds);
      expect(response.body.friends[0].id).toEqual(FRIEND.id);
      expect(response.body.friends[0].name).toEqual(FRIEND.name);
    });
  });
});
