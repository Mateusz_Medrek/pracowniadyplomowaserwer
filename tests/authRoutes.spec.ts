jest.mock('google-auth-library');

import '../src/models/User';

import { OAuth2Client } from 'google-auth-library';
import mockingoose from 'mockingoose';
import { model, Types } from 'mongoose';
import request from 'supertest';

import { AUTH_ROUTES } from '../src/consts';
import { Providers, UserModel } from '../src/models/types';
import authRoutes from '../src/routes/auth';
import app from '../src/services/appConfig';

const NAME = 'ABCD';
const SOME_EMAIL = 'someEmail@gmail.com';
const SOME_INVALID_EMAIL = 'someInvalidEmail@gmail.com';
const SOME_INVALID_EMAIL_FORMAT = 'someInvalidEmailFormat';
const SOME_PASSWORD = 'somePassword';
const SOME_INVALID_PASSWORD = 'someInvalidPassword';
const USER_ID = '0123456789ABCDEF01234567';

const MOCK_VALID_GOOGLE_TOKEN = 'IAmValidGoogleToken';
const MOCK_VALIDPASSWORD = 'validPassword';
const MOCK_SOME_UNIQUE_GOOGLE_ID_FROM_TOKEN = '1234567890';

const verifyIdToken = jest.fn(
  ({ idToken, audience }: { idToken: string; audience: string }) => {
    if (idToken === MOCK_VALID_GOOGLE_TOKEN) {
      return {
        getPayload: jest.fn(() => ({
          aud: audience,
          exp: 123,
          iss: 'accounts.google.com',
          sub: MOCK_SOME_UNIQUE_GOOGLE_ID_FROM_TOKEN,
          email: 'someEmail@gmail.com',
        })),
      };
    } else {
      return {
        getPayload: jest.fn(() => undefined),
      };
    }
  }
);

jest.mock('jsonwebtoken', () => ({
  sign: jest.fn(() => {
    return 'IAmToken';
  }),
}));

jest.mock('bcrypt', () => ({
  compare: jest.fn(
    (
      candidate: string,
      realPassword: string,
      callback: (err: Error | undefined, same: boolean) => void
    ) => {
      if (candidate !== MOCK_VALIDPASSWORD) {
        callback(undefined, false);
      } else {
        callback(undefined, true);
      }
    }
  ),
  genSalt: jest.fn((rounds, callback) => {
    callback(undefined, '98765');
  }),
  hash: jest.fn((data, salt, callback) => {
    callback(undefined, MOCK_VALIDPASSWORD);
  }),
}));

const User = model<UserModel>('User');

app.use(authRoutes);

describe('authRoutes', () => {
  describe(`/${AUTH_ROUTES.SIGN_IN}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when email or password are not provided', async () => {
      let response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_IN}`)
        .send({ email: SOME_EMAIL, password: '' })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual(
        'Email and password must be provided'
      );
      response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_IN}`)
        .send({ email: '', password: SOME_PASSWORD })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual(
        'Email and password must be provided'
      );
    });
    it('should return error when email has invalid format', async () => {
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_IN}`)
        .send({
          email: SOME_INVALID_EMAIL_FORMAT,
          password: SOME_PASSWORD,
        })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual('Email has invalid format');
    });
    it('should return error when email is invalid', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_IN}`)
        .send({
          email: SOME_INVALID_EMAIL,
          password: SOME_PASSWORD,
        })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual('Invalid email');
    });
    it('should return error when password is invalid', async () => {
      mockingoose(User).toReturn({ password: 'someHashedPassword' }, 'findOne');
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_IN}`)
        .send({
          email: SOME_EMAIL,
          password: SOME_INVALID_PASSWORD,
        })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual('Invalid password');
    });
    it('should return token when email and password are valid', async () => {
      mockingoose(User).toReturn(
        {
          _id: new Types.ObjectId(USER_ID),
          email: SOME_EMAIL,
          password: MOCK_VALIDPASSWORD,
          name: NAME,
          accountType: Providers.email,
        },
        'findOne'
      );
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_IN}`)
        .send({ email: SOME_EMAIL, password: MOCK_VALIDPASSWORD })
        .set('Accept', 'application/json');
      expect(response.text).toEqual('IAmToken');
    });
  });
  describe(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when email or password, or name are not provided', async () => {
      let response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`)
        .send({ email: SOME_EMAIL, password: '', name: NAME })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual(
        'Email, password and name must be provided'
      );
      response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`)
        .send({ email: '', password: SOME_PASSWORD, name: NAME })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual(
        'Email, password and name must be provided'
      );
      response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`)
        .send({
          email: SOME_EMAIL,
          password: SOME_PASSWORD,
          name: '',
        })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual(
        'Email, password and name must be provided'
      );
    });
    it('should return error when email has invalid format', async () => {
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`)
        .send({
          email: SOME_INVALID_EMAIL_FORMAT,
          password: SOME_PASSWORD,
          name: NAME,
        })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual('Email has invalid format');
    });
    it('should return error when there is already account with this email', async () => {
      mockingoose(User).toReturn(
        {
          _id: new Types.ObjectId(USER_ID),
          email: SOME_EMAIL,
          password: SOME_PASSWORD,
          name: NAME,
          accountType: Providers.email,
        },
        'findOne'
      );
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`)
        .send({
          email: SOME_EMAIL,
          password: SOME_PASSWORD,
          name: NAME,
        })
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual(
        'There is already account with this email'
      );
    });
    it('should return token', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      mockingoose(User).toReturn(
        {
          _id: new Types.ObjectId(USER_ID),
          email: SOME_EMAIL,
          password: SOME_PASSWORD,
          name: NAME,
          accountType: Providers.email,
        },
        'save'
      );
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_EMAIL}`)
        .send({
          email: SOME_EMAIL,
          password: MOCK_VALIDPASSWORD,
          name: NAME,
        })
        .set('Accept', 'application/json');
      expect(response.text).toEqual('IAmToken');
    });
  });
  describe(`/${AUTH_ROUTES.SIGN_UP_GOOGLE}`, () => {
    beforeAll(() => {
      (OAuth2Client as any).mockImplementation(() => ({
        verifyIdToken,
      }));
    });
    beforeEach(() => {
      (OAuth2Client as any).mockClear();
      mockingoose.resetAll();
      verifyIdToken.mockClear();
    });
    it('should return error when googletoken is not provided', async () => {
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_GOOGLE}`)
        .set('Accept', 'application/json');
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual('Not authenticated');
    });
    it('should return error when googletoken is invalid', async () => {
      const invalidgoogletoken = 'IAmInvalidGoogleToken';
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_GOOGLE}`)
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + invalidgoogletoken);
      expect(response.status).toEqual(401);
      expect(response.body.error).toEqual('Not authenticated');
    });
    it('should return googletoken when user does not have an account and it is created', async () => {
      mockingoose(User)
        .toReturn(undefined, 'findOne')
        .toReturn(
          {
            id: Providers.google + MOCK_SOME_UNIQUE_GOOGLE_ID_FROM_TOKEN,
            accountType: Providers.google,
            email: SOME_EMAIL,
            name: NAME,
          },
          'save'
        );
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_GOOGLE}`)
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + MOCK_VALID_GOOGLE_TOKEN);
      expect(response.text).toEqual(MOCK_VALID_GOOGLE_TOKEN);
    });
    it('should return googletoken when user has already an account', async () => {
      mockingoose(User).toReturn(
        {
          id: Providers.google + MOCK_SOME_UNIQUE_GOOGLE_ID_FROM_TOKEN,
          accountType: Providers.google,
          email: SOME_EMAIL,
          name: NAME,
        },
        'findOne'
      );
      const response = await request(app)
        .post(`/${AUTH_ROUTES.SIGN_UP_GOOGLE}`)
        .set('Accept', 'application/json')
        .set('Authorization', 'Bearer ' + MOCK_VALID_GOOGLE_TOKEN);
      expect(response.text).toEqual(MOCK_VALID_GOOGLE_TOKEN);
    });
  });
});
