import '../src/models/Task';
import '../src/models/User';

import { Request, Response, NextFunction } from 'express';
import mockingoose from 'mockingoose';
import { model, Types } from 'mongoose';
import request from 'supertest';

import { TASK_ROUTES } from '../src/consts';
import { TaskModel, UserModel, Providers, SubTask } from '../src/models/types';
import taskRoutes from '../src/routes/task';
import app from '../src/services/appConfig';

const Task = model<TaskModel>('Task');
const User = model<UserModel>('User');

const DESCRIPTION = 'EFGH';
const NAME = 'ABCDEF';
const SOME_EMAIL = 'someEmail@gmail.com';
const TASK_ID = '76543210FEDCBA9876543210';
const USER_ID = '0123456789ABCDEF01234567';
const SUBTASK: SubTask = {
  assigneeId: USER_ID,
  category: 1,
  isCompleted: false,
  id: 13,
  title: 'ABCDEF',
};
const TITLE = 'ABCD';
const USER = {
  _id: new Types.ObjectId(USER_ID),
  name: NAME,
  accountType: Providers.email,
  email: SOME_EMAIL,
  id: USER_ID,
  tasksIds: [] as string[],
};
const MOCK_VALID_TOKEN = 'Bearer 0IAmValidToken';

jest.mock('../src/middlewares/requireAuth', () =>
  jest.fn((req: Request, res: Response, next: NextFunction) => {
    if (req.headers.authorization === MOCK_VALID_TOKEN) {
      return next();
    } else {
      return res.status(401).send({ error: 'Not authenticated' });
    }
  })
);

jest.mock('jsonwebtoken', () => ({
  verify: jest.fn((token, secret, callback) => {
    if (token === 'IAmValidToken') {
      callback(undefined, { userId: '0skdjvbs21984bisdbvi9232f' });
    } else {
      callback(undefined, {});
    }
  }),
}));

app.use(taskRoutes);

describe('taskRoutes', () => {
  const bodyDate = new Date();
  const bodyDate2 = new Date();
  bodyDate.setFullYear(bodyDate.getFullYear() - 1);
  bodyDate2.setFullYear(bodyDate2.getFullYear() + 1);
  const body = {
    title: TITLE,
    description: DESCRIPTION,
    startingTime: bodyDate,
    finishingTime: bodyDate2,
    subTasks: [SUBTASK],
    isCompleted: false,
  };
  describe(`/${TASK_ROUTES.CREATE_TASK}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when any of body arguments is not provided', async () => {
      let response = await request(app)
        .post(`/${TASK_ROUTES.CREATE_TASK}`)
        .send({ ...body, title: '' })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.body.error).toEqual('Arguments missing');
      response = await request(app)
        .post(`/${TASK_ROUTES.CREATE_TASK}`)
        .send({ ...body, description: '' })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.body.error).toEqual('Arguments missing');
      response = await request(app)
        .post(`/${TASK_ROUTES.CREATE_TASK}`)
        .send({ ...body, startingTime: undefined })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.body.error).toEqual('Arguments missing');
      response = await request(app)
        .post(`/${TASK_ROUTES.CREATE_TASK}`)
        .send({ ...body, finishingTime: undefined })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.body.error).toEqual('Arguments missing');
      mockingoose(Task).toReturn(
        {
          _id: 'MongoID1234',
          ...body,
        },
        'save'
      );
    });
    it('should return OK when task is created', async () => {
      mockingoose(Task).toReturn(
        {
          _id: new Types.ObjectId(TASK_ID),
          ...body,
        },
        'save'
      );
      mockingoose(User).toReturn(USER, 'findOne');
      const updatedUser = {
        ...USER,
      };
      updatedUser.tasksIds.push();
      mockingoose(User).toReturn(
        {
          _id: new Types.ObjectId(USER_ID),
          name: NAME,
          accountType: Providers.email,
          email: SOME_EMAIL,
          id: USER_ID,
          tasksIds: [TASK_ID],
        },
        'save'
      );
      const response = await request(app)
        .post(`/${TASK_ROUTES.CREATE_TASK}`)
        .send({ ...body })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${TASK_ROUTES.CHECK_TASK}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no task in database with given id', async () => {
      mockingoose(Task).toReturn(undefined, 'findOneAndUpdate');
      const response = await request(app)
        .post(`/${TASK_ROUTES.CHECK_TASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no task in database with given id'
      );
    });
    it('should return OK after checking task', async () => {
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      mockingoose(Task).toReturn(_doc, 'findOneAndUpdate');
      const response = await request(app)
        .post(`/${TASK_ROUTES.CHECK_TASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${TASK_ROUTES.CHECK_SUBTASK}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no task in database with given id', async () => {
      mockingoose(Task).toReturn(undefined, 'findOne');
      const response = await request(app)
        .post(`/${TASK_ROUTES.CHECK_SUBTASK}`)
        .send({ id: TASK_ID, subTaskId: SUBTASK.id })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no task in database with given id'
      );
    });
    it('should return error when there is no subtask in provided task', async () => {
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      mockingoose(Task).toReturn(_doc, 'findOne');
      const response = await request(app)
        .post(`/${TASK_ROUTES.CHECK_SUBTASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('There is no subtask in provided task');
    });
    it('should return OK after checking subtask', async () => {
      const subTaskId = body.subTasks[0].id;
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      mockingoose(Task).toReturn(_doc, 'findOne');
      _doc.subTasks[0].isCompleted = true;
      mockingoose(Task).toReturn(_doc, 'save');
      const response = await request(app)
        .post(`/${TASK_ROUTES.CHECK_SUBTASK}`)
        .send({ id: TASK_ID, subTaskId })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${TASK_ROUTES.DELETE_TASK}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no task in database with given id', async () => {
      mockingoose(Task).toReturn(undefined, 'findOneAndDelete');
      const response = await request(app)
        .post(`/${TASK_ROUTES.DELETE_TASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no task in database with given id'
      );
    });
    it('should return OK after successful delete', async () => {
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      const userWithTask = {
        ...USER,
      };
      userWithTask.tasksIds.push(TASK_ID);
      const users = [userWithTask];
      mockingoose(Task).toReturn(_doc, 'findOneAndDelete');
      mockingoose(User).toReturn(users, 'find');
      users[0].tasksIds.pop();
      mockingoose(User).toReturn(users, 'save');
      const response = await request(app)
        .post(`/${TASK_ROUTES.DELETE_TASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${TASK_ROUTES.EDIT_TASK}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no task in database with this id', async () => {
      mockingoose(Task).toReturn(undefined, 'findOne');
      const updatedTask = {
        id: TASK_ID,
        ...body,
      };
      const response = await request(app)
        .post(`/${TASK_ROUTES.EDIT_TASK}`)
        .send(updatedTask)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no task in database with given id'
      );
    });
    it('should return OK after successful task update', async () => {
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      const _doc2 = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      _doc2.description = 'BLABLABLABLABLA';
      const updatedTask = {
        id: TASK_ID,
        ..._doc2,
      };
      mockingoose(Task).toReturn(_doc, 'findOne');
      mockingoose(Task).toReturn(_doc2, 'save');
      const response = await request(app)
        .post(`/${TASK_ROUTES.EDIT_TASK}`)
        .send(updatedTask)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${TASK_ROUTES.GET_ALL_COMPLETED_TASKS}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no user in database with given id', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_ALL_COMPLETED_TASKS}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no user in database with given id'
      );
    });
    it('should return list of tasks names', async () => {
      const body2 = {
        title: TITLE,
        description: DESCRIPTION,
        startingTime: bodyDate,
        finishingTime: bodyDate2,
        subTasks: [SUBTASK],
        isCompleted: true,
      };
      const userWithTask = {
        _id: new Types.ObjectId(USER_ID),
        name: NAME,
        accountType: Providers.email,
        email: SOME_EMAIL,
        id: USER_ID,
        tasksIds: [] as string[],
      };
      userWithTask.tasksIds.push(TASK_ID);
      mockingoose(User).toReturn(userWithTask, 'findOne');
      const tasks = [
        {
          _id: new Types.ObjectId(TASK_ID),
          ...body2,
        },
      ];
      mockingoose(Task).toReturn(tasks, 'find');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_ALL_COMPLETED_TASKS}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.tasks).toHaveLength(tasks.length);
    });
  });
  describe(`/${TASK_ROUTES.GET_ALL_TASKS}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no user in database with given id', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_ALL_TASKS}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no user in database with given id'
      );
    });
    it('should return list of tasks names', async () => {
      const userWithTask = {
        _id: new Types.ObjectId(USER_ID),
        name: NAME,
        accountType: Providers.email,
        email: SOME_EMAIL,
        id: USER_ID,
        tasksIds: [] as string[],
      };
      userWithTask.tasksIds.push(TASK_ID);
      mockingoose(User).toReturn(userWithTask, 'findOne');
      const tasks = [
        {
          _id: new Types.ObjectId(TASK_ID),
          ...body,
        },
      ];
      mockingoose(Task).toReturn(tasks, 'find');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_ALL_TASKS}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.tasks).toHaveLength(tasks.length);
    });
  });
  describe(`/${TASK_ROUTES.GET_ALL_TODAY_TASKS}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no user in database with given id', async () => {
      mockingoose(User).toReturn(undefined, 'findOne');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_ALL_TODAY_TASKS}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no user in database with given id'
      );
    });
    it('should return list of tasks names', async () => {
      const userWithTasks = {
        ...USER,
      };
      userWithTasks.tasksIds.push(TASK_ID);
      mockingoose(User).toReturn(userWithTasks, 'findOne');
      const tasks = [
        {
          _id: new Types.ObjectId(TASK_ID),
          ...body,
        },
      ];
      mockingoose(Task).toReturn(tasks, 'find');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_ALL_TODAY_TASKS}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.tasks).toHaveLength(tasks.length);
    });
  });
  describe(`/${TASK_ROUTES.GET_TASK}/:id`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no task in database with given id', async () => {
      mockingoose(Task).toReturn(undefined, 'findOne');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_TASK}/${TASK_ID}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no task in database with given id'
      );
    });
    it('should return object with task properties', async () => {
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      mockingoose(Task).toReturn(_doc, 'findOne');
      const response = await request(app)
        .get(`/${TASK_ROUTES.GET_TASK}/${TASK_ID}`)
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
      expect(response.body.title).toEqual(body.title);
      expect(response.body.description).toEqual(body.description);
      expect(response.body.startingTime).toEqual(
        body.startingTime.toISOString()
      );
      expect(response.body.finishingTime).toEqual(
        body.finishingTime.toISOString()
      );
      expect(response.body.subTasks[0]).toEqual(body.subTasks[0]);
    });
  });
  describe(`/${TASK_ROUTES.UNCHECK_TASK}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no task in database with given id', async () => {
      mockingoose(Task).toReturn(undefined, 'findOneAndUpdate');
      const response = await request(app)
        .post(`/${TASK_ROUTES.UNCHECK_TASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no task in database with given id'
      );
    });
    it('should return OK after unchecking task', async () => {
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      mockingoose(Task).toReturn(_doc, 'findOneAndUpdate');
      const response = await request(app)
        .post(`/${TASK_ROUTES.UNCHECK_TASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
  describe(`/${TASK_ROUTES.UNCHECK_SUBTASK}`, () => {
    beforeEach(() => {
      mockingoose.resetAll();
    });
    it('should return error when there is no task in database with given id', async () => {
      mockingoose(Task).toReturn(undefined, 'findOne');
      const response = await request(app)
        .post(`/${TASK_ROUTES.UNCHECK_SUBTASK}`)
        .send({ id: TASK_ID, subTaskId: SUBTASK.id })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual(
        'There is no task in database with given id'
      );
    });
    it('should return error when there is no subtask in provided task', async () => {
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      _doc.subTasks[0].isCompleted = true;
      mockingoose(Task).toReturn(_doc, 'findOne');
      const response = await request(app)
        .post(`/${TASK_ROUTES.UNCHECK_SUBTASK}`)
        .send({ id: TASK_ID })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('There is no subtask in provided task');
    });
    it('should return OK after unchecking subtask', async () => {
      const subTaskId = body.subTasks[0].id;
      const _doc = {
        _id: new Types.ObjectId(TASK_ID),
        ...body,
      };
      _doc.subTasks[0].isCompleted = true;
      mockingoose(Task).toReturn(_doc, 'findOne');
      _doc.subTasks[0].isCompleted = false;
      mockingoose(Task).toReturn(_doc, 'save');
      const response = await request(app)
        .post(`/${TASK_ROUTES.UNCHECK_SUBTASK}`)
        .send({ id: TASK_ID, subTaskId })
        .set('Accept', 'application/json')
        .set('Authorization', MOCK_VALID_TOKEN);
      expect(response.status).toEqual(200);
    });
  });
});
