jest.mock('google-auth-library');

import { OAuth2Client } from 'google-auth-library';

import httpMocks from 'node-mocks-http';
import wait from 'waait';

import requireAuth from '../src/middlewares/requireAuth';

const INVALID_EMAIL_TOKEN = 'INVALID_EMAIL_TOKEN';
const MOCK_VALID_EMAIL_TOKEN = 'VALID_EMAIL_TOKEN';
const INVALID_GOOGLE_TOKEN = 'INVALID_GOOGLE_TOKEN';
const MOCK_VALID_GOOGLE_TOKEN = 'VALID_GOOGLE_TOKEN';

const verifyIdToken = jest.fn(
  ({ idToken, audience }: { idToken: string; audience: string }) => {
    if (idToken === MOCK_VALID_GOOGLE_TOKEN) {
      return {
        getPayload: jest.fn(() => ({
          aud: audience,
          exp: 123,
          iss: 'accounts.google.com',
        })),
      };
    } else {
      return {
        getPayload: jest.fn(() => undefined),
      };
    }
  }
);

jest.mock('jsonwebtoken', () => ({
  verify: jest.fn((token, secret, callback) => {
    if (token === MOCK_VALID_EMAIL_TOKEN) {
      callback(undefined, { userId: '1234' });
    } else {
      callback(undefined, {});
    }
  }),
}));

describe('requireAuth', () => {
  describe('without token', () => {
    it('should return error when authorization header is not present', () => {
      const req = httpMocks.createRequest({ headers: {} });
      const res = httpMocks.createResponse({});
      const next = jest.fn();
      requireAuth(req, res, next);
      expect(res.statusCode).toEqual(401);
      expect(res._getData().error).toBe('Not authenticated');
    });
  });
  describe('no provider', () => {
    it('should return error when there is no provider in token', async () => {
      const accessToken = 'Bearer IamTokenWithoutProvider';
      const req = httpMocks.createRequest({
        headers: {
          authorization: accessToken,
        },
      });
      const res = httpMocks.createResponse({});
      const next = jest.fn();
      requireAuth(req, res, next);
      await wait(0);
      expect(res.statusCode).toEqual(401);
      expect(res._getData().error).toBe('No provider');
    });
  });
  describe('email provider', () => {
    it('should return error when token is invalid', async () => {
      const accessToken = 'Bearer 0' + INVALID_EMAIL_TOKEN;
      const req = httpMocks.createRequest({
        headers: {
          authorization: accessToken,
        },
      });
      const res = httpMocks.createResponse({});
      const next = jest.fn();
      requireAuth(req, res, next);
      await wait(0);
      expect(res.statusCode).toEqual(401);
      expect(res._getData().error).toBe('Not authenticated');
    });
    it('should call next function when token is valid', async () => {
      const accessToken = 'Bearer 0' + MOCK_VALID_EMAIL_TOKEN;
      const req = httpMocks.createRequest({
        headers: {
          authorization: accessToken,
        },
      });
      const res = httpMocks.createResponse({});
      const next = jest.fn();
      requireAuth(req, res, next);
      await wait(0);
      expect(next).toBeCalledTimes(1);
    });
  });
  describe('google provider', () => {
    beforeAll(() => {
      (OAuth2Client as any).mockImplementation(() => ({
        verifyIdToken,
      }));
    });
    beforeEach(() => {
      (OAuth2Client as any).mockClear();
      verifyIdToken.mockClear();
    });
    it('should return error when token is invalid', async () => {
      const accessToken = 'Bearer 2' + INVALID_GOOGLE_TOKEN;
      const req = httpMocks.createRequest({
        headers: {
          authorization: accessToken,
        },
      });
      const res = httpMocks.createResponse({});
      const next = jest.fn();
      requireAuth(req, res, next);
      await wait(0);
      expect(res.statusCode).toEqual(401);
      expect(res._getData().error).toBe('Not authenticated');
    });
    it('should call next function when token is valid', async () => {
      const accessToken = 'Bearer 2' + MOCK_VALID_GOOGLE_TOKEN;
      const req = httpMocks.createRequest({
        headers: {
          authorization: accessToken,
        },
      });
      const res = httpMocks.createResponse({});
      const next = jest.fn();
      requireAuth(req, res, next);
      await wait(0);
      expect(next).toBeCalledTimes(1);
    });
  });
});
