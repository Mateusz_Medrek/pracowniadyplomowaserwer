import '../src/models/Task';
import '../src/models/User';

import mockingoose from 'mockingoose';
import { model, Types } from 'mongoose';
import wait from 'waait';

import { NODEMAILER_USERNAME } from '../config/keys';
import { TaskModel, UserModel, Providers } from '../src/models/types';
import transporter from '../src/services/nodemailerConfig';
import sendEmails from '../src/utils/sendEmails';

jest.mock('../src/services/nodemailerConfig.ts', () => ({
  sendMail: jest.fn(),
}));

const EMAIL = 'friendEmail@gmail.com';
const ID = '0skdjvbs21984bisdbvi9232f';
const NAME = 'ABCDEF';
const TASK_ID = '76543210FEDCBA9876543210';
const DESCRIPTION = 'EFGH';
const USER_ID = '0123456789ABCDEF01234567';
const SUBTASK = {
  assignee: USER_ID,
  category: 1,
  isCompleted: false,
  id: 13,
  title: 'ABCDEF',
};
const TITLE = 'ABCD';

const Task = model<TaskModel>('Task');
const User = model<UserModel>('User');

const _userDoc = {
  id: ID,
  name: NAME,
  email: EMAIL,
  accountType: Providers.email,
  tasksIds: [TASK_ID],
  friends: new Map<string, boolean>(),
};
const bodyDate = new Date();
const bodyDate2 = new Date();
bodyDate.setFullYear(bodyDate.getFullYear() - 3);
bodyDate2.setFullYear(bodyDate2.getFullYear() + 5);
const body = {
  title: TITLE,
  description: DESCRIPTION,
  startingTime: bodyDate,
  finishingTime: bodyDate2,
  subTasks: [SUBTASK],
  isCompleted: false,
};

describe('sendEmails', () => {
  beforeEach(() => {
    mockingoose.resetAll();
    (transporter.sendMail as any).mockClear();
  });
  it('should call transporter.sendMail method with options object', async () => {
    mockingoose(User).toReturn([_userDoc], 'find');
    const _taskDoc = {
      _id: new Types.ObjectId(TASK_ID),
      ...body,
    };
    mockingoose(Task).toReturn(_taskDoc, 'findOne');
    await sendEmails();
    expect(transporter.sendMail).toBeCalledWith({
      from: `"ToDoApp" ${NODEMAILER_USERNAME}`,
      to: _userDoc.email,
      subject: `You have ${_userDoc.tasksIds.length} today`,
      html:
        '<h3>Check your to-do list and have a nice day!</h3>' +
        '<h4>Your tasks are:</h4>' +
        `<ul><li>${_taskDoc.title}</li></ul>`,
    });
  });
  // it('should not call transporter.sendMail when all tasks are completed', async () => {
  //   mockingoose(User).toReturn([_userDoc], 'find');
  //   const _taskDoc = {
  //     _id: new Types.ObjectId(TASK_ID),
  //     ...body,
  //     isCompleted: true,
  //   };
  //   mockingoose(Task).toReturn(_taskDoc, 'findOne');
  //   sendEmails();
  //   await wait(0);
  //   expect(transporter.sendMail).toBeCalledTimes(0);
  // });
  // it('should not call transporter.sendMail for tasks which did not start', async () => {
  //   mockingoose(User).toReturn([_userDoc], 'find');
  //   const newStartingTime = new Date();
  //   newStartingTime.setFullYear(newStartingTime.getFullYear() + 1);
  //   const _taskDoc = {
  //     _id: new Types.ObjectId(TASK_ID),
  //     ...body,
  //     startingTime: newStartingTime,
  //   };
  //   mockingoose(Task).toReturn(_taskDoc, 'findOne');
  //   sendEmails();
  //   await wait(0);
  //   expect(transporter.sendMail).toBeCalledTimes(0);
  // });
});
