module.exports = {
  singleQuote: true,
  tabWidth: 2,
  trailingComma: true,
};
